#!/bin/sh
# decode-token.sh - decode a WLCG token and print requested aspects
# v1.3 - 2023/05/12
# author M. Litmaath

ttl=
times=

case $1 in
-d)
    ttl=1
    shift
    ;;
-t)
    times=1
    shift
    ;;
-?*)
    echo "Usage: $0 [-d | -t] [WLCG_token_file | standard input]" >&2
    exit 2
esac

token=$(cat "${1:--}" | cut -d. -f2)

[ ${token:+x} ] || {
    echo "ERROR: no token found in ${1:-stdin}" >&2
    exit 1
}

case $((${#token} % 4)) in
2)
    token+='=='
    ;;
3)
    token+='='
esac

#
# as the real "jq" command is not necessarily available,
# this approximation should do for the time being...
#

jq()
{
    perl -e '
	$i = "";

	sub p
	{
	    $c = shift;

	    if ($c eq ",") {
		return "$c\n$i";
	    } elsif ($c =~ /[[{]/) {
		$i =~ s/$/  /;
		return "$c\n$i";
	    } elsif ($c =~ /[]}]/) {
		$i =~ s/  $//;
		return "\n$i$c";
	    }

	    return $c;
	}

	while (<>) {
	    s/":/": /g;
	    s-\\/-/-g;
	    s/./&p($&)/eg;
	    print;
	}

	print "\n";
    '
}

printf '%s' "$token" | tr '_-' '/+' | base64 --decode | jq . | (
    if [ $ttl ]
    then
	perl -ne '
	    /"exp"\s*:\s*(\d+)/ or next;
	    $ttl = $1 - time;
	    print $ttl > 0 ? "$ttl\n" : "0\n";
	    exit;
	' | grep . || {
	    echo "ERROR: the token TTL could not be determined" >&2
	    exit 1;
	}
    elif [ $times ]
    then
	perl -ne '
	    /"(nbf|exp)"\s*:\s*(\d+)/ or next;
	    print "$1: " . localtime($2) . "\n";
	' | grep . || {
	    echo "ERROR: the token times could not be determined" >&2
	    exit 1;
	}
    else
	perl -pe '
	    /^[{}]/ and $n++;
	    /^  ".+": .*,$/ and $n++;
	    END {
		exit($n < 7);
	    }
	' || {
	    echo "ERROR: the token contents could not be determined" >&2
	    exit 1;
	}
    fi
)

