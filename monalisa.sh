#!/bin/bash

# JAliEn VObox Startup Scripts - MonaLisa
# v1.13 - 2022-02-07
# Authors:
#     Kalana Dananjaya <kwijethu@cern.ch>
#     Costin  Grigoras <Costin.Grigoras@cern.ch>
#     Maarten Litmaath <Maarten.Litmaath@cern.ch>

mlPlatformDir=/cvmfs/alice.cern.ch/el7-x86_64
monalisaDir=$mlPlatformDir/Packages/MonALISA
mlModuleDir=$mlPlatformDir/Modules/modulefiles/MonALISA

########################################################################################
# Write log to file
# Globals:
#   setupLogFile: Log file for MonaLisa setup
# Arguments:
#   $1: String to log
########################################################################################
function write_log(){
	echo $1 >> $setupLogFile
}

########################################################################################
# Templates a given configuration files(Add, delete or change content)
# Globals:
#   add: Array of lines to add to file
#	rmv: Array of lines to be removed from file
#	changes: Associative array of line to be changed {original_text:change_to_text}
# Arguments:
#   srcFile: Source file
#	destFile: Destination file
########################################################################################
function template(){

	srcFile=$1
	destFile=$2

	write_log "=========== Templating the File ==========="
	write_log "+ Source File: $srcFile"
	write_log "+ Destination File: $destFile"
	write_log ""

	# Create backup of the original file if it exists
	[ -f $destFile ] && cp -f $destFile "$destFile.orig"
	cp $srcFile $destFile

	# Apply lines changes
	write_log ">>> Applying Changes"
	for key in "${!changes[@]}"
	do
		# Find partial matches and replace

		v=$(
			printf "%s" "${changes[$key]}" |
			perl -p00e 's/\n/ /g;s/[\\|]/\\$&/g'
		)

		write_log "Change key: $key to value: $v"

		sed -i "s|$key|$v|" $destFile
	done
	unset changes && write_log ""

	# Append new Lines
	write_log ">>> Adding new lines"
	for i in "${add[@]}"
	do
		write_log "+++ $i"
		echo "$i" >> $destFile
	done
	unset add && write_log ""

	# Remove existing lines
	write_log ">>> Removing existing lines"
	for i in "${rmv[@]}"
	do
		# Find exact word matches and delete the line
		write_log "--- $i"
		sed -i "/$i\b/d" $destFile
	done
	unset rmv && write_log ""

	write_log " --- Templating Complete ---"
	write_log ""

}

########################################################################################
# Setup MonaLisa
# Globals:
#   monalisaLDAPconfiguration: Associative array of MonaLisa configuration parameters in LDAP
#	baseLogDir: MonaLisa Log file. Defaults to ~/ALICE/alien-logs
#	ceLogFile: CE Log File
# Arguments:
#   farmHome: MonaLisa base package location
#	logDir: MonaLisa log directory
########################################################################################
function setup() {

	farmHome=$1
	logDir=$2

	add=()
	rmv=()
	declare -Ag changes

	# Copy base templates to the local directory
	mkdir -p "$logDir/myFarm/"

	# ==============================================================================
	# myFarm.conf

	# Convert multi-valued attribute to array
	while IFS= read -r line
	do
		add+=("$line")
	done <<< "${monalisaLDAPconfiguration[ADDMODULES]}"

	add+=("^monLogTail{Cluster=AliEnServicesLogs,Node=CE,command=tail -n 15 -F $ceLogFile 2>&1}%3")
	cmd=/cvmfs/alice.cern.ch/scripts/vobox/jalien_vobox_services
	add+=("*AliEnTestsStatus{monStatusCmd, localhost, \"logDir=$baseLogDir $cmd,timeout=800\"}%900")
	add+=("*LCGServicesStatus{monStatusCmd, localhost, \"logDir=$baseLogDir $cmd -lcg,timeout=800\"}%900")

	template "$farmHome/Service/myFarm/myFarm.conf" "$logDir/myFarm/myFarm.conf"

	# ==============================================================================
	# ml.properties

	declare -Ag changes

	# Convert multi-valued attribute to array
	while IFS= read -r line
	do
		add+=("$line")
	done <<< "${monalisaLDAPconfiguration[ADDPROPERTIES]}"


	for attr in location country latitude longitude administrator
	do
		k=${attr^^}
		eval "$attr=\${monalisaLDAPconfiguration[$k]:-\${siteConfiguration[$k]}}"
	done

	changes["^MonaLisa.Location.*"]="MonaLisa.Location=$location"
	changes["^MonaLisa.Country.*"]="MonaLisa.Country=$country"
	changes["^MonaLisa.LAT.*"]="MonaLisa.LAT=$latitude"
	changes["^MonaLisa.LONG.*"]="MonaLisa.LONG=$longitude"
	changes["^MonaLisa.ContactEmail.*"]="MonaLisa.ContactEmail=$administrator"

	template "$farmHome/Service/myFarm/ml.properties" "$logDir/myFarm/ml.properties"

	# =============================================================================
	# ml.env

	declare -Ag changes
	changes["^FARM_NAME.*"]="FARM_NAME=\"${monalisaLDAPconfiguration[NAME]}\""
	changes["^FARM_HOME.*"]="FARM_HOME=\"$logDir/myFarm\""
	changes["^MONALISA_USER.*"]="MONALISA_USER=\"$(id -u -n)\""
	changes["^MonaLisa_HOME.*"]="MonaLisa_HOME=\"$MonaLisa_HOME\""

	template "$farmHome/Service/CMD/ml_env" "$logDir/myFarm/ml_env"

	# ============================= Export variables ==============================
	export CONFDIR="$logDir/myFarm"
	export ALICE_LOGDIR=$baseLogDir
	export JAVA_OPTS=${monalisaLDAPconfiguration[JAVAOPTS]}
}

########################################################################################
# MonaLisa liveness check
# Returns:
#   0 if process is running,else 1
########################################################################################
function check_liveness_ml(){

	pid=$(pgrep -n -u `id -u` -f -- "-DMonaLisa_HOME=")
	if [[ -z $pid ]]
	then
		return 1
	else
		return 0
	fi
}

########################################################################################
# Start MonaLisa
# Globals:
#	hostConfiguration: Associative array of host configuration parameters in LDAP
#	siteConfiguration: Associative array of site configuration parameters in LDAP
#	monalisaLDAPconfiguration: Associative array of MonaLisa configuration parameters in LDAP
#   commonConfiguration: Associative array of JAliEn local configuration parameters
# Arguments:
#   confDir: AliEn configuration directory
#	ldapHostname: LDAP hostname
#	ldapPort: LDAP port
#	hostname: Site hostname
########################################################################################
function start_ml(){

	# Check if there is an existing instance
	check_liveness_ml

	if [[ $? == 0 ]]
	then
		echo "MonaLisa already running"
		return 0
	fi

	nl='
	'
	nl=${nl:0:1}

	echo "Configuring MonaLisa..."

	# ==================== Config generation from LDAP ====================

	ldapBase="ou=Sites,o=alice,dc=cern,dc=ch"
	ldapFilter="(&(objectClass=AliEnHostConfig)(host=$hostname))"

	hostLDAPQuery=$(
		ldapsearch -x -LLL -H ldap://$ldapHostname:$ldapPort -b $ldapBase "$ldapFilter" |
		perl -p00e 's/\n //g' | envsubst
	)

	declare -A hostConfiguration

	while IFS= read -r line
	do
		if [[ ! -z $line ]]
		then
			key=$(echo "$line" | cut -d ":" -f 1 )
			val=$(echo "$line" | cut -d ":" -f 2- | sed s/.//)

			key=${key^^}
			prev=${hostConfiguration[$key]}
			prev=${prev:+$prev$nl}

			hostConfiguration[$key]=$prev$val
		fi
	done <<< "$hostLDAPQuery"

	parentSite=${hostConfiguration[DN]%,ou=Sites,*}
	parentSite=${parentSite##*=}
	ldapBase="ou=$parentSite,ou=Sites,o=alice,dc=cern,dc=ch"
	ldapFilter="(&(objectClass=AliEnSite))"

	siteLDAPQuery=$(
		ldapsearch -x -LLL -H ldap://$ldapHostname:$ldapPort -b $ldapBase "$ldapFilter" |
		perl -p00e 's/\n //g' | envsubst
	)

	declare -A siteConfiguration

	while IFS= read -r line
	do
		if [[ ! -z $line ]]
		then
			key=$(echo "$line" | cut -d ":" -f 1 )
			val=$(echo "$line" | cut -d ":" -f 2- | sed s/.//)

			key=${key^^}
			prev=${siteConfiguration[$key]}
			prev=${prev:+$prev$nl}

			siteConfiguration[$key]=$prev$val
		fi
	done <<< "$siteLDAPQuery"

	ldapFilter="(&(objectClass=AliEnMonaLisa)(host=$hostname))"

	monalisaLDAPQuery=$(
		ldapsearch -x -LLL -H ldap://$ldapHostname:$ldapPort -b $ldapBase "$ldapFilter" |
		perl -p00e 's/\n //g' | envsubst
	)

	declare -A monalisaLDAPconfiguration

	while IFS= read -r line
	do
	if [[ ! -z $line ]]
		then

		key=$(echo "$line" | cut -d ":" -f 1)
		val=$(echo "$line" | cut -d ":" -f 2- | sed s/.//)

		key=${key^^}
		prev=${monalisaLDAPconfiguration[$key]}
		prev=${prev:+$prev$nl}

		monalisaLDAPconfiguration[$key]=$prev$val
	fi
	done <<< "$monalisaLDAPQuery"

	case ${monalisaLDAPconfiguration[NAME]} in
	'')
		echo "LDAP Configuration for MonaLisa not found. Please set it up and try again."
		exit 1
		;;

	LCG|LCG[-_]*)
		monalisaLDAPconfiguration[NAME]=$parentSite${monalisaLDAPconfiguration[NAME]#LCG}
	esac

	baseLogDir=$(echo "${hostConfiguration[LOGDIR]}" | envsubst)

	if [[ -z $baseLogDir ]]
	then
		baseLogDir="$HOME/ALICE/alien-logs"
		echo "LDAP doesn't define a particular log location, using the default ($baseLogDir)"
	fi

	logDir="$baseLogDir/MonaLisa"

	if ! mkdir -p $logDir
	then
		echo "Unable to create log directory at $logDir"
		return 1
	fi

	setupLogFile="$logDir/ML-config-inputs.txt"
	ceLogFile="$baseLogDir/CE.log.0"
	envFile="$logDir/ml-env.sh"
	commonConf="$confDir/version.properties"
	mlEnv="$confDir/ml.env"
	envCommand="/cvmfs/alice.cern.ch/bin/alienv printenv MonALISA"

	> $setupLogFile

	write_log "========== Site Config =========="
	for x in "${!siteConfiguration[@]}"
	do
		printf "[%s]=%s\n" "$x" "${siteConfiguration[$x]}" >> $setupLogFile
	done
	write_log ""

	write_log "========== VObox Config =========="
	for x in "${!hostConfiguration[@]}"
	do
		printf "[%s]=%s\n" "$x" "${hostConfiguration[$x]}" >> $setupLogFile
	done
	write_log ""

	write_log "========== MonaLisa Config =========="
	for x in "${!monalisaLDAPconfiguration[@]}"
	do
		printf "[%s]=%s\n" "$x" "${monalisaLDAPconfiguration[$x]}" >> $setupLogFile
	done
	write_log ""


	# Read MonaLisa config files
	if [[ -f "$commonConf" ]]
	then
		declare -A commonConfiguration

		while IFS= read -r line
		do
		if [[ ! "$line" = \#* ]] && [[ "$line" =~ [A-Za-z].*= ]]
		then
			key=$(echo "$line" | cut -d "=" -f 1 )
			val=$(echo "$line" | cut -d "=" -f 2- )

			key=${key^^}
			prev=${commonConfiguration[$key]}
			prev=${prev:+$prev$nl}

			commonConfiguration[$key]=$prev$val
		fi
		done < "$commonConf"
	fi

	write_log ""
	write_log "========== Local Configuration start =========="
	for x in "${!commonConfiguration[@]}"
	do
		printf "[%s]=%s\n" "$x" "${commonConfiguration[$x]}" >> $setupLogFile
	done
	write_log "========== Local Configuration end =========="
	write_log ""

	# Reset the environment
	> $envFile

	# Bootstrap the environment e.g. with the correct X509_USER_PROXY
	[[ -f "$mlEnv" ]] && cat "$mlEnv" >> $envFile

	# If a custom MonaLisa package is declared, use that package as MonaLisa_HOME
	if [[ -n "${commonConfiguration[MONALISA_HOME]}" ]]
	then
		d=${commonConfiguration[MONALISA_HOME]}

		if ! [[ "$d" =~ / ]]
		then
			d=${d/MonaLisa-}

			[[ "$d" =~ [0-9] ]] || d=20211013

			d=MonaLisa-$d
			f=$d.tar.gz

			echo "Downloading MonaLisa tar ball into $HOME/$f"

			curl -s -S -o "$HOME/$f" "http://alimonitor.cern.ch/download/MonaLisa/$f" &&
			tar -xf "$HOME/$f" -C $HOME &&
			perl -i.$$ -pe "s|^\s*(MONALISA_HOME)\s*=.*|\$1=$HOME/$d|i" "$commonConf" || exit 1

			commonConfiguration[MONALISA_HOME]=$HOME/$d
		fi

		echo "export MonaLisa_HOME=${commonConfiguration[MONALISA_HOME]};" >> $envFile
	else
		# If a specific MonaLisa package is declared, use that, else the "pro" version

		mlv=${commonConfiguration[MONALISA]:-pro}

		monalisaVersion=$(
			p=$mlModuleDir/$mlv
			(eval readlink "$p" || eval ls -ld "$p") | sed '$!d;s-.*/--'
		)

		if [[ -z "$monalisaVersion" ]]
		then
			echo "Requested MonaLisa version '$mlv' could not be resolved"
			return 1
		fi

		envCommand="$envCommand/$monalisaVersion"

		$envCommand | grep ^ >> $envFile || exit 1

		echo "export MonaLisa_HOME=$monalisaDir/$monalisaVersion;" >> $envFile
	fi

	(
		#
		# change environment and directory only in this subshell...
		#

		source $envFile

		farmHome=${MonaLisa_HOME}

		if [[ -z $farmHome ]]
		then
			echo "Please point MonaLisa_HOME to the MonaLisa package location,"
			echo "possibly by defining it in the version.properties file"
			exit 1
		fi

		# ==================== Start templating config files ====================

		setup $farmHome $logDir

		echo "Starting MonaLisa...."

		cd $logDir
		$farmHome/Service/CMD/ML_SER start < /dev/null
	)

	#
	# Let cron watch over MonALISA...
	#
	# NOTE: for a local installation it is crucial that
	# the code has not changed directory in this shell !
	#

	manage_crontab 7-57/10 monalisa

	s=3

	for i in 1 2 3
	do
	    sleep $s
	    status_ml  && break
	    [ $i = 3 ] && break
	    echo Checking again in $s seconds...
	done
}

########################################################################################
# Stop MonaLisa
########################################################################################
function stop_ml(){

	#
	# first prevent cron from restarting MonALISA...
	#

	manage_crontab remove monalisa

	echo "Stopping MonaLisa..."

	for pid in $(pgrep -u `id -u` -f -- '-DMonaLisa_HOME=')
	do
		# request children to shutdown
		kill -s HUP $pid &>/dev/null
		echo -n "."; sleep 1
		kill -s HUP $pid &>/dev/null
		echo -n "."; sleep 1
		kill -s TERM $pid &>/dev/null
		echo -n "."; sleep 2
	done

	check_liveness_ml
	exit_code=$?

	if [[ $exit_code == 0 ]]
	then
		echo ' MonaLisa still running!'
	else
		echo ' MonaLisa stopped'
	fi
}

########################################################################################
# Check MonaLisa status
# Arguments:
#   $command: "status" or "mlstatus"
# Returns:
#	0 if process is running,else 1
########################################################################################
function status_ml() {
	command=$1

	check_liveness_ml
	exit_code=$?

	[[ $exit_code == 0 ]] && not= || not=' Not'

	if [[ "$command" == mlstatus ]]
	then
		echo -e "MonaLisa\tStatus\t$exit_code\tMessage\tMonaLisa$not Running"
	else
		echo -e "MonaLisa$not Running"
	fi

	return $exit_code
}

########################################################################################
# Check MonaLisa
# Globals:
#	checkLog : the logfile for the check results
########################################################################################
function check_ml() {
	(
		echo "=== START `date` PID $$"

		if [ -e ~/no_check_monalisa ]
		then
			echo "Not checking MonALISA"
		else
			status_ml || start_ml
		fi

		echo "=== READY `date` PID $$"

	) 2>&1 < /dev/null | tee -a $checkLog
}

########################################################################################
# Run command
# Arguments:
#	command to execute
########################################################################################

function run_monalisa() {
	command=$1

	if [[ $command = "start" ]]
	then
		start_ml

	elif [[ $command = "stop" ]]
	then
		stop_ml

	elif [[ $command = "restart" ]]
	then
		stop_ml
		t=10
		echo additional grace period of $t seconds...
		sleep $t
		start_ml

	elif [[ $command = "condrestart" ]]
	then
		status_ml && run_monalisa restart

	elif [[ $command =~ "status" ]]
	then
		status_ml $command

	elif [[ $command = "check" ]]
	then
		check_ml
	else
		echo "Error: unknown command: $command" >&2
		return 2
	fi
}
