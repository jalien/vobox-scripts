#!/bin/bash

# JAliEn VObox Startup Scripts - CE
# v1.26 - 2024-05-28
# Authors:
#     Kalana Dananjaya <kwijethu@cern.ch>
#     Costin  Grigoras <Costin.Grigoras@cern.ch>
#     Maarten Litmaath <Maarten.Litmaath@cern.ch>

ceClassName=alien.site.ComputingElement
jPlatformDir=/cvmfs/alice.cern.ch/el[79]-x86_64   # matches el7 and el9 builds
jalienDir=$jPlatformDir/Packages/JAliEn
jModuleDir=$jPlatformDir/Modules/modulefiles/JAliEn
commonConf="$confDir/version.properties"

export JALIEN_CONF_DIR=${JALIEN_CONF_DIR:-$HOME/.alien/config}

########################################################################################
# Write log to file
# Globals:
#   setupLogFile: Log file for CE setup
# Arguments:
#   $1: String to log
########################################################################################
function write_log() {
	echo "$@" >> $setupLogFile
}

########################################################################################
# Stop CE
# Globals:
#	ceClassName : Computing Element classname
########################################################################################
function stop_ce() {
	#
	# first prevent cron from restarting the CE...
	#

	manage_crontab remove ce

	echo "Stopping JAliEn CE..."
	pkill -TERM -f $ceClassName
	sleep 3

	status_ce || return 0

	echo "Killing JAliEn CE..."
	pkill -KILL -f $ceClassName

	! status_ce
}

########################################################################################
# CE PID
# Prints:
#   PID if process is running, else nothing
# Returns:
#   0 if process is running, else 1
########################################################################################
function get_ce_pid() {
	ps fuxwww | awk "/java .*[ ]$ceClassName/ { print \$2 }" | sed '$!d' | grep .
}

########################################################################################
# CE liveness check
# Returns:
#   0 if process is running, else 1
########################################################################################
function check_liveness_ce() {
	if get_ce_pid > /dev/null
	then
		return 0
	else
		return 1
	fi
}

########################################################################################
# Check CE status
# Arguments:
#	$command: "status" or "mlstatus"
# Returns: 
#	0 if process is running, else 1
########################################################################################
function status_ce() {
	command=$1

	pid=$(get_ce_pid)
	exit_code=$?

	[[ $exit_code == 0 ]] && not= || not=' Not'

	if [[ "$command" == mlstatus ]]
	then
		ceConfigVersion=$(
			[[ -f "$commonConf" ]] &&
			perl -ne 'print $1 if /^\s*jalien\s*=\s*(\S+)/i' $commonConf
		)

		: ${ceConfigVersion:=pro}

		if [[ -n "$pid" ]]
		then
			ceEnvRunningVersion=$(
		    		(tr \\0 \\n < /proc/"$pid"/environ) 2> /dev/null |
				perl -ne "
					BEGIN {
						\$v = 'local';
					}
					if (m,^CLASSPATH=.*?$jalienDir/([^/:]+),) {
						\$v=\$1;
					}
					END {
						print \$v if \$.;
					}
				"
			)
		else
			ceEnvRunningVersion=
		fi

		jaVersion=
		jconf=$JALIEN_CONF_DIR/version.properties

		if [[ -e $jconf ]]
		then
			jaVersion=$(perl -ne 'print $1 if /^\s*jobagent\.version\s*=\s*(\S+)/i' $jconf)
			custom=$(perl -ne 'print $2 if /^\s*custom\.jobagent\.jar(\.dir)?\s*=\s*(\S+)/i' $jconf)

			if [[ "x$custom" != x ]]
			then
				jaVersion=custom
			fi
		fi

		jaVersion=${jaVersion:-${ceEnvRunningVersion:-$ceConfigVersion}}

		msg="CE\tStatus\t$exit_code\tMessage\tCE$not Running"
		msg="$msg\tConfigVersion\t$ceConfigVersion"
		msg="$msg\tRunningVersion\t${ceEnvRunningVersion:-n/a}"
		msg="$msg\tJAVersion\t$jaVersion"

		echo -e "$msg"
	else
		echo -e "CE$not Running"
	fi

	return $exit_code
}

########################################################################################
# Start CE
# Globals:
#	TBD
########################################################################################
function start_ce() {
	nl='
	'
	nl=${nl:0:1}

	if check_liveness_ce
	then
		echo "JAliEn CE already running"
		return 0
	fi

	echo "Configuring the JAliEn CE..."

	#
	# Obtain host configuration from LDAP
	#

	ldapBase="ou=Sites,o=alice,dc=cern,dc=ch"
	ldapFilter="(&(objectClass=AliEnHostConfig)(host=$hostname))"

	hostLDAPQuery=$(
		ldapsearch -x -LLL -H ldap://$ldapHostname:$ldapPort -b $ldapBase "$ldapFilter" |
		perl -p00e 's/\n //g' | envsubst
	)

	declare -A hostConfiguration

	while IFS= read -r line
	do
		#
		# Create an associative array from the LDAP configuration
		#

		if [[ ! -z $line ]]
		then
			key=$(echo "$line" | cut -d ":" -f 1 )
			val=$(echo "$line" | cut -d ":" -f 2- | sed s/.//)

			key=${key^^}
			prev=${hostConfiguration[$key]}
			prev=${prev:+$prev$nl}

			hostConfiguration[$key]=$prev$val
		fi
	done <<< "$hostLDAPQuery"

	baseLogDir=$(echo "${hostConfiguration[LOGDIR]}" | envsubst)

	if [[ -z $baseLogDir ]]
	then
		baseLogDir="$HOME/ALICE/alien-logs"
		echo "LDAP doesn't define a particular log location, using the default ($baseLogDir)"
	fi

	logDir="$baseLogDir/CE"

	if ! mkdir -p $logDir
	then
		echo "Unable to create log directory at $logDir"
		return 1
	fi

	setupLogFile="$logDir/CE-config-inputs.txt"
	ceEnv="$confDir/CE.env"
	envCommand="/cvmfs/alice.cern.ch/bin/alienv printenv JAliEn"

	> $setupLogFile

	#
	# Read JAliEn VObox config file
	#

	if [[ -f "$commonConf" ]]
	then
		declare -A commonConfiguration

		while IFS= read -r line
		do
			if [[ ! "$line" = \#* ]] && [[ "$line" =~ [A-Za-z].*= ]]
			then
				key=$(echo "$line" | cut -d "=" -f 1 )
				val=$(echo "$line" | cut -d "=" -f 2- )

				key=${key^^}
				prev=${commonConfiguration[$key]}
				prev=${prev:+$prev$nl}

				commonConfiguration[$key]=$prev$val
			fi
		done < "$commonConf"
	fi

	write_log ""
	write_log "===================== Local Configuration start ==================="
	for x in "${!commonConfiguration[@]}"
	do
		printf "[%s]=%s\n" "$x" "${commonConfiguration[$x]}" >> $setupLogFile
	done
	write_log "===================== Local Configuration end ==================="
	write_log ""

	envFile="$logDir/CE-env.sh"
	pidFile="$logDir/CE.pid"

	#
	# Reset the CE environment
	#

	> $envFile

	#
	# Bootstrap the CE environment e.g. with the correct X509_USER_PROXY
	#

	[[ -f "$ceEnv" ]] && cat "$ceEnv" >> $envFile

	#
	# Check for JAliEn version
	#

	jv=${commonConfiguration[JALIEN]}

	[[ -n "$jv" ]] || jv=pro

	jalienVersion=$(
		p=$jModuleDir/$jv
		(eval readlink -f "$p" || eval ls -ld "$p") | sed '$!d;s-.*/--'
	)

	if [[ -z "$jalienVersion" ]]
	then
		echo "Requested JAliEn version '$jv' could not be resolved"
		return 1
	fi

	envCommand="$envCommand/$jalienVersion"

	#
	# ensure a modified PATH will end up in the JAliEn environment...
	#

	([[ -f "$ceEnv" ]] && source "$ceEnv"; $envCommand) | grep ^ >> $envFile || return 1

	#
	# All systems go...
	#

	logFile="$logDir/CE-jvm-$(date '+%y%m%d-%H%M%S')-$$-log.txt"

	echo "Starting the JAliEn CE..."

	(
		#
		# change environment and directory only in this subshell...
		#

		#
		# in particular avoid inheriting JAVA_OPTS from MonALISA,
		# when it is instructed to restart the CE...
		#

		unset $(env | sed -n 's/^\(JAVA[^=]*\)=.*/\1/p')

		source $envFile

		cd $logDir
		nohup jalien $ceClassName > "$logFile" 2>&1 < /dev/null &
		echo $! > "$pidFile"
	)

	#
	# Let cron watch over the CE...
	#
	# NOTE: for a local installation it is crucial that
	# the code has not changed directory in this shell !
	#

	manage_crontab 3-53/10 ce

	s=3
	n=3

	for i in `seq $n`
	do
	    sleep $s
	    status_ce   && return 0
	    [ $i = $n ] && return 1
	    echo Checking again in $s seconds...
	done
}

########################################################################################
# Check CE
# Globals:
#	checkLog : the logfile for the check results
########################################################################################
function check_ce() {
	(
		echo "=== START `date` PID $$"

		if [ -e ~/no_check_ce ]
		then
			echo "Not checking the CE"
		else
			status_ce || start_ce
		fi

		echo "=== READY `date` PID $$"

	) 2>&1 < /dev/null | tee -a $checkLog
}

########################################################################################
# Run command
# Arguments:
#	command to execute
########################################################################################
function run_ce() {
	command=$1

	if [[ $command = "start" ]]
	then
		start_ce

	elif [[ $command = "stop" ]]
	then
		stop_ce

	elif [[ $command = "restart" ]]
	then
		if status_ce mlstatus | grep -w local
		then
			echo "Leaving the LOCAL version running!"
		else
			stop_ce
			sleep 3
			start_ce
		fi

	elif [[ $command = "condrestart" ]]
	then
		status_ce && run_ce restart

	elif [[ $command =~ "status" ]]
	then
		status_ce $command

	elif [[ $command = "check" ]]
	then
		check_ce
	else
		echo "Error: unknown command: $command" >&2
		return 2
	fi
}
