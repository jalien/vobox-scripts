#!/bin/bash

# JAliEn VObox Startup Scripts - wrapper
# v1.27 - 2025-02-22
# Authors:
#     Kalana Dananjaya <kwijethu@cern.ch>
#     Costin  Grigoras <Costin.Grigoras@cern.ch>
#     Maarten Litmaath <Maarten.Litmaath@cern.ch>

# 
# when this script is called from MonALISA,
# the environment needs to be cleaned up first,
# to avoid dynamic linking errors in particular
# 

pfx=/cvmfs/alice.cern.ch/

case :$LD_LIBRARY_PATH in
*:$pfx*)
    export LD_LIBRARY_PATH=$(
	echo "$LD_LIBRARY_PATH" |
	LD_LIBRARY_PATH= perl -pe "s,$pfx[^:]+:?,,g"
    )
esac

ldapHostname="alice-ldap.cern.ch"
ldapPort="8389"
hostname=`hostname -f`
confDir="${HOME}/.alien/config"
checkLog=/tmp/vobox-check-`date +%y%m%d`.log

[ -L "$0" ] && p=$(readlink -f "$0") || p=$0

dir=$(dirname -- "$p")

source $dir/jalien-ce.sh
source $dir/monalisa.sh

cmds='start status stop restart condrestart mlstatus check'
svcs[0]='ce'
svcs[1]='monalisa'

usage()
{
	exec >&2
	echo ""
	echo "Usage: $0 <Command> [<Service> ...]"
	echo ""
	echo "<Command> is one of: $cmds"
	echo "<Service> is one of: ${svcs[*]} (defaulting to '${svcs[0]}' if not specified)"
	echo ""
	exit 2
}

cron_job()
{
	if [ $# -lt 1 ] || [ $# -gt 2 ]
	then
		echo "Usage: ${FUNCNAME[0]} remove_pattern [ cron-entry-string ]" >&2
		return 2
	fi

	local rm_pat=$1
	local entry=$2

	(
		crontab -l | egrep -v -e "$rm_pat"

		[ ${entry:+x} ] && printf '%s\n' "$entry"

	) | crontab
}

manage_crontab()
{
	usage="Usage: ${FUNCNAME[0]} { minute_spec | remove } service"

	case $1 in
	*[0-9]*)
		if [ "x${1/[^-0-9,\/}" != "x$1" ]
		then
			echo "Error: bad minute specifier: $1" >&2
			echo "$usage" >&2
			return 2
		fi

		local min=$1
		;;
	remove)
		local min=
		;;
	*)
		echo "$usage" >&2
		return 2
	esac

	case " ${svcs[*]} " in
	*" $2 "*)
		local svc=$2
		;;
	*)
		echo "Error: unsupported service: $2" >&2
		echo "Supported services are: ${svcs[*]}" >&2
		echo "$usage" >&2
		return 2
	esac

	case $0 in
	/*)
		local cmd=$0
		;;
	./*)
		local cmd=$PWD${0#.}
		;;
	*)
		local cmd=$PWD/$0
	esac

	local entry=

	if [ ${min:+x} ]
	then
		entry="$min * * * * /bin/sh $cmd check $svc > /dev/null 2>&1 < /dev/null"
	fi

	cron_job "${cmd##*/} check $svc" "$entry"
}


for cmd in $cmds
do
	[[ "x$1" = x$cmd ]] && break
done

[[ "x$1" = x$cmd ]] || usage

shift

[[ $# = 0 ]] && set ${svcs[0]}

for arg
do
	for svc in ${svcs[*]}
	do
		[[ "x$arg" = x$svc ]] && break
	done
	
	[[ "x$arg" = x$svc ]] || usage
done

rc=0

for svc
do
	if [[ $svc = "monalisa" ]]
	then
		(run_monalisa $cmd) || rc=$?

	elif [[ $svc = "ce" ]]
	then
		(run_ce $cmd) || rc=$?
	else 
		usage
	fi
done

exit $rc

