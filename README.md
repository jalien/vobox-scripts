# JAliEn Startup Scripts

JAliEn startup scripts are used to control CE and MonaLisa services on VOboxes. The production versions reside in `/cvmfs/alice.cern.ch/scripts/vobox`.

# Usage
``` 
Usage: jalien-vobox.sh <Command> [<Service> ...]

<Command> is one of: start status stop restart condrestart mlstatus check

<Service> is one of: ce monalisa (defaulting to 'ce' if not specified)
```

Running a command without specifying a service will run the command just on the CE service.

## Configuration
The following parameters can be overridden by defining them in `$HOME/.alien/config/version.properties` as key-value pairs, but their (unspecified) default values should usually be fine:

```
#MONALISA_HOME=<MonaLisa package location>
#MonaLisa=yyyymmdd-r
#jalien=x.y.z-r
#jobagent.version=x.y.z-r
#custom.jobagent.jar.dir=/cvmfs/alice.cern.ch/java/jar/jalien
```

> Note: MONALISA_HOME would typically be used to specify a `local` installation. The value `install` can be used to have a default version downloaded and configured.

If it is required to run any shell command before starting either of the services, they can be added in `$HOME/.alien/config/CE.env` or `$HOME/.alien/config/ml.env`. Both files should typically contain a command to define the `X509_USER_PROXY` **if needed** for job submission by the JAliEn CE and which should then be monitored by MonALISA, for example as follows:

```
export X509_USER_PROXY=$(ls -t /var/lib/vobox/alice/proxy_repository/*lcgadmin | sed q)
export MYPROXY_SERVER=myproxy.cern.ch   # only needed for MonALISA
```

> Note: if the JAliEn CE does **not need** an X509 proxy, please ensure those variables are **not defined** for the services, to avoid false alarms in the VObox monitoring tests!

> Note: the JAliEn CE needs a valid `VObox token` (typically provided through `~/.globus/host{cert,key}.pem`) to be able to submit jobs.

> Note: the JAliEn CE **may** need a valid `WLCG token` in `~/.globus/wlcg.dat` to be able to submit jobs to site CE services.

## Job script customization

The job script submitted by the JAliEn CE (typically `~/ALICE/tmp/agent.startup.$PID`) can be customized with one or more scripts located in the home directory:

* `~/JA-custom-0.sh` - Contents get inserted before the JAliEn environment setup.
* `~/JA-custom-1.sh` - Contents get inserted right before JAliEn is started.
* `~/JA-custom-2.sh` - Contents get inserted right after JAliEn has finished.

Such customizations are in use at some sites to deal with special conditions and can also be handy for debugging.

